SELECT  anime.nom, anime.note, ANIME.id
FROM ANIME
JOIN hero
ON hero.id_anime = anime.id
GROUP BY anime.id
HAVING count(hero.id_anime) = 2