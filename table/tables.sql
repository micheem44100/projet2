CREATE TABLE ANIME(id NUMERIC PRIMARY KEY, nom TEXT, synopsis TEXT, date_de_sortie INT, genre TEXT, studio text, id_auteur REFERENCES Auteur(id));

CREATE table film(id NUMERIC PRIMARY KEY, nom TEXT, synopsis text, date_de_sortie INT, note int, recette int, id_anime REFERENCES ANIME(id));

CREATE TABLE Auteur(id NUMERIC PRIMARY KEY, nom TEXT(30), prenom TEXT(30), année_de_naissance NUMERIC(5));

Create TABLE hero(id NUMERIC PRIMARY KEY, prénom TEXT, race TEXT, age INT, couleur_de_cheveux TEXT, sexe TEXT, id_anime REFERENCES ANIME(id));


