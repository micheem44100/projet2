INSERT INTO hero VALUES			
(1,'Son Goku', 'Saiyan' , 38, 'noir', 'homme', 1),

(2,'Naruto', 'humain' , 16, 'blond', 'homme', 2),	
			
(3,'Guts', 'humain' , 24, 'noir', 'homme', 3),

(4,'Luffy', 'humain' , 19, 'noir', 'homme', 4),
			
(5,'Katsuji', 'humain' , 42, 'noir', 'homme', 5),

(6,'Tanjiro', 'humain' , 15, 'rouge foncé', 'homme', 13),

(7,'Sakura', 'humain', 16, 'rose', 'femme', 2),

(8,'Nezuko', 'mi-humain mi-démon', 14, 'noir', 'femme', 6),

(9,'Aralé', 'gynoïde', 13, 'violet', 'femme', 7),

(10,'Saitama', 'humain', 25, 'chauve', 'homme', 15),

(11, 'humain/quincy/hollow/chinigami', 'Ichigo Kurosaki', 15,'roux', 'homme', 8),

(12, 'humain', 'Eren', 18, 'noir', 'homme', 9),

(13, 'humain/ghoul', 'Kaneki', 29,'noir', 'homme', 10),

(14, 'humain', 'Sailor Moon', 14,'blond', 'femme', 11),

(15, 'humain', 'Yuji', 15,'noir/rose', 'homme', 12),

(16, 'mi-humain mi-demon', 'Denji', 16,'blond', 'homme', 14),

(17, 'humain', 'Koruko', 17,'bleu', 'homme', 16),

(18, 'humain', 'Asta', 15,'gris', 'homme', 17),

(19, 'humain', 'Isagi', 16,'noir', 'homme', 18),

(20, 'humain', 'Izuku',  14,'vert', 'homme', 19);

