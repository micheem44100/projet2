INSERT INTO AUTEUR(id, nom, prenom, année_de_naissance, sexe) VALUES


(1,'Akira', 'Toriyama' , 1955, 'homme'),
					
					
(2,'Masashi', 'Kishimoto' , 1974, 'homme'),

			
(3,'Kentaro', 'Muira' , 1966, 'homme'),


(4,'Eiichiro', 'Oda' , 1955, 'homme'),


(5,'Koyoharu ', 'Gotōge' , 1989, 'homme'),


(6, 'Kubo', 'Tite', 1974, 'homme'),

(7, 'Isayama', 'Hajime ', 1986, 'homme'),

(8, 'Ishida', 'Sui', 1986, 'homme'),

(9, 'Takeuchi', 'Naoko', 1967, 'femme'),

(10, 'Akutami', 'Gege', 1992, 'homme'),

(11, 'Fujimoto', 'Tatsuki', 1992, 'homme'),

(12, 'ONE', 'ONE', 1986, 'homme'),

(13, 'Fujimaki', 'Tadatoshi', 1982, 'homme'),

(14, 'Tabatai', 'Yūki', 1984, 'homme'),

(15, 'Kaneshiro', 'Muneyuki', 1966, 'homme'),

(16, 'Horikoshi', 'Kōhei', 1986, 'homme');