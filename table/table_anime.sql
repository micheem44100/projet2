INSERT INTO ANIME VALUES
(1,'Dragon Ball Z', 'Lhistoire de Dragon Ball suit la vie de Son Goku, un garçon à la queue de singe inspiré du conte traditionnel chinois La Pérégrination vers lOuest. Son Goku est un jeune garçon simple desprit et pur doté dune queue de singe et dune force extraordinaire.'
					,1989, 'Shonen Nekketsu', 1, 0),
			
(2, 'Naruto Shippuden', 'Naruto est un jeune ninja du village de Konoha. Hôte du démon renard à neuf queues, une créature qui a attaqué le village par le passé, il est rejeté par les autres villageois. Son ambition est de devenir Hokage, le chef du village, afin de gagner le respect des habitants.'
					, 2007, 'Shonen Nekketsu', 2, 1),

(3, 'Berserk', 'Dans un monde médiéval et fantastique, erre un guerrier solitaire nommé Guts, décidé à être seul maître de son destin.'
					,1997,  'Seinen', 3, 8),

(4, 'One Piece', 'Monkey D. Luffy rêve de retrouver ce trésor légendaire et de devenir le nouveau "Roi des Pirates". Après avoir mangé un fruit du démon, il possède un pouvoir lui permettant de réaliser son rêve. Il lui faut maintenant trouver un équipage pour partir à laventure !'
					,1999, 'Shonen Nekketsu', 4, 1),
					
(5,'Japan', 'Lhistoire commence en 1992 où un chef Yakuza, Katsuji Yashima, a suivi une journaliste japonaise, Yuka Katsuragi, à Barcelone car il veut lépouser. Ils sont devant la Sagrada Familia pour un reportage lorsque tout dun coup un tremblement de terre les jette dans des ruines.'
					,1992, 'Seinen', 3, NULL),

(6,'Demon slayer', 'Tanjiro mène une vie modeste mais heureuse dans les montagnes avec sa famille. Un jour, lorsquil revient de la vente de charbon de bois en ville, il trouve les restes de sa famille massacrée dans des flaques de sang après une attaque de démon.'
					,2019, 'Shonen Nekketsu', 5, 4),

(7, 'Dr Slump', 'Le Docteur Slump crée une gynoïde qui a les traits dune jeune fille de 13 ans et décide de la tester en linsérant dans la vie du village Pingouin en la présentant comme sa sœur Aralé. Cependant, celle-ci possède des supers pouvoirs (force, vitesse, intelligence) ainsi quune grande naïveté, et va chambouler la vie du village habité par des êtres quelque peu extravagants…'
					,1980, 'Shonen', 1, 0),

(8, 'Bleach', 'Les aventures de Ichigo Kurosaki et ses amies protegeant la terre contre les forces du mal', 2003, 'Shonen Nekketsu', 6, 1),

(9, 'Attack on titan', 'Les hommes vivent protégés de titans mangeur d homme par des grands murs', 2013, 'Shonen Nekketsu', 7, 5),			
		
(10,'Jujutsu Kaisen', 'Le héros de Jujutsu Kaisen, Yuji Itadori, un lycéen, se retrouve confronté aux forces occultes et est enrôlé dans une école dexorcisme le jour où il se voit possédé par le démon millénaire Ryomen Sukuna.', 2018, 'Shonen Nekketsu', 10, 1),

(11, 'Tokyo Ghoul', 'Kaneki ken devient malgré lui une ghoul et doit apprendre à vivre avec', 2011, 'Seinen', 8, 1),

(12, 'Sailor Moon', 'Les aventures de Sailor Moon et des autres Sailor protègeant la Terre', 1992, 'Shonen Nekketsu', 9, 0),

(13, 'Demon slayer', 'Les aventures de Tanjiro essayant de trouver un moyen de rendre à nouveau humaine sa soeur devenu une demon', 2016, 'Shonen Nekketsu', 5, 4),

(14, 'Chainsaw man', 'Les aventures de Denji devenu un demon tronçonneuse', 2018, 'Shonen Nekketsu', 11, 5),

(15, 'One punch man', 'Les aventures de Saitama qui bat tous ses adversaire en un seul coups', 2012, 'Shonen Nekketsu', 12, 10),

(16, 'Kuruko no basket', 'Les aventures de Kuruko et ses coéquipiers dans le championnat interlycée', 2008, 'Shonen Nekketsu', 13, 9),

(17, 'Black Clover', 'Les aventures de Asta et Yuno pour devenir empreur mage', 2015, 'Shonen Nekketsu', 14, 1),

(18, 'Blue lock', 'Le Japon tente un nouveau programme, le blue lock, pour former le meilleur buteur au monde afin gagner la coupe du monde', 2018, 'Shonen', 15, 6),

(19, 'My hero academia', 'Les aventures de Midoriya Izuku pour devenir le hero numéro 1', 2014, 'Shonen', 16, 2);

