INSERT INTO FILM VALUES
(1,'Cent Mille Guerriers de métal', 'Cooler est expédié dans lespace et pratiquement détruit, mais un étrange corps extra-terrestre prend son cerveau et part en direction de la planète Namek. Son Goku et ses amis décident de partir pour Namek où ils découvrent le peuple réduit à lesclavage par des robots. Cest alors quapparaît un robot à leffigie de Cooler.',
					1992, 7, 18000000, 1),
		
(2,'La Revanche de Cooler', 'Personne ne le savait mais Freezer le défunt a un frère du nom de Cooler. Il débarque sur la terre pour venger son frère et exterminer tous les Saiyens',
					1991, 8, 16000000, 1),
			
(3,'The Last', 'L’aventure racontée dans the Last se déroule deux ans après la quatrième grande guerre ninja. Nos héros, qui savourent une paix bien méritée depuis, s’apprêtent à célébrer la fête de l’Hiver. Malheureusement pour eux, ils doivent rapidement faire face à un nouveau danger : la Lune se rapproche de la planète de façon inquiétante et des météorites, bris de l’astre qui se désintègre peu à peu, commencent à s’écraser sur le sol. Pour ne rien arranger, le grand méchant du film, un mystérieux personnage du nom de Toneri, fait son apparition.',
					2015, 9, 13000000, 2),

(4, 'Stampede', 'Luffy et son équipage sont sur le point de participer au plus grand rassemblement mondial de pirates : le Pirate Fest, organisé par le machiavélique Buena Festa.',
					2019, 10, 38000000, 4),

(5,'Le train de linfinie', 'Tanjirô et Nezuko, accompagnés de Zenitsu et Inosuke, sallient à lun des plus puissants épéistes de larmée des pourfendeurs de démons, le Pilier de la Flamme Kyôjurô Rengoku, afin de contrer le démon qui a engagé le train de lInfini sur une voie funeste.',
					2021, 10, 500000000, 6),

(6, 'Dragon Ball Super super hero', 'Gohan et Picollo font face au retour du Ruban Rouge qui cache une arme destructrice très dangereuse', 2022, 7, 43508272.95, 1),

(7, 'Dragon Ball Super Broly','Goku et Végéta affrontent Broly, un saiyan voulant se venger des deux', 2018, 8, 57748590.58, 1),

(8, 'Dragon Ball Z la résurection de Freezer', 'Sorbet et Tagoma, deux membres de larmée de Freezer, sont à la recherche des boules de cristal pour ressusciter le terrible tyran.', 2015, 6, 3752579.52, 1),

(9, 'Dragon Ball Z batle of gods','une fois de plus en péril car Beerus, le dieu de la destruction, se réveille d une longue torpeur et décide de partir à la recherche du Super Saïyen divin', 2014, 9, 3319589.58, 1),

(10, 'One Piece red', 'Luffy revoit une vielle amie d enfance qui a secrètement un ojectif sombre envers le monde', 2018, 7, 53402093.18, 4),

(11, 'One Piece gold','luffy et son équipage sont invités dans une vile en or, qui cache un terrible secret', 2014, 8, 2757424.3, 4),

(12, 'Jujustu Kaisen 0', 'Yuta possédé par son amie Rika fait la rencontre de Gojo', 2021, 8, 16374380.74, 10),

(13, 'Kuruko no Basket last game', 'La génération miracle affronte les meilleurs joueurs de la NBA', 2017, 9, 1449073.02, 16),

(14, 'My Hero Academia two heroes', 'Izuku et All Might reçoivent une invitation d’une certaine personne, pour une immense ville ambulante appelée « I-Island »', 2019, 8, 6206189.21, 19),

(15, 'My Hero Academia world heroes mission', 'Considérant les Alters comme un fléau, l’organisation Humarise a décidé d’éradiquer les détenteurs de ces pouvoirs de la surface du globe', 2021, 8, 18741248.11, 19);
