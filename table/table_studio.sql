INSERT INTO STUDIO(id, nom, fondateur, date_de_creation, effectif) VALUES
(0, 'La Toei animation', 'Kenzō Masaoka', 'janvier 1948', 819),
(1, 'Studio Pierrot', 'Hisayuki Toriumi', 'mai 1979', 158),
(2, 'Bones', 'Masahiko Minami', ' octobre 1998', 80),
(4, 'Ufotable', 'Hikaru Kondō', 'octobre 2000', 219),
(5, 'Mappa', 'Masao Maruyama', 'juin 2011', 310),
(6, '8-Bits', 'Tsutomu Kasai', 'septembre 2008',91),
(7, 'Liden Films', 'Tetsurō Satomi', 'février 2012', 160),
(8, 'OLM studio', 'Kunihiko Yuyama', 'juin 1994', 216),
(9, 'Production I.G', 'Mitsuhisa Ishikawa', 'décembre 1987', 219),
(10, 'Madhouse', 'Masao Maruyama', 'octobre 1972', 70);